<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:191',
            'body' => 'min:2',
            'password' => 'required|min:6|max:191',
        ]);

        if ($request->has('body')) {
            Post::create([
                'name' => $request->request->get('name'),
                'body' => encrypt($request->request->get('body')),
                'password' => bcrypt($request->request->get('password')),
            ]);

            return 'Saved successfully.';
        } else {
            $post = Post::where('name', $request->request->get('name'))->firstOrFail();

            if (\Hash::check($request->request->get('password'), $post->password)) {
                return decrypt($post->body);
            } else {
                return 'Wrong password';
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }
}
